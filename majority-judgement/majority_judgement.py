import numpy as np
import sys
import argparse
from termcolor import colored
from getpass import getpass

COLORS = ["green", "blue", "cyan", "yellow", "red", "grey"]
N_COLUMNS = 51


class Interface:
    def read(self):  # cli
        parser = argparse.ArgumentParser()
        parser.add_argument(
            "-f",
            "--file",
            help="Input file mode. The format of the input file is described in the documentation.",
        )
        parser.add_argument(
            "-i",
            "--interactive",
            action="store_true",
            help="Interactive mode. Use this mode if you wish to type the input using the console.",
        )
        args = parser.parse_args()
        if not (args.file or args.interactive):
            parser.error(
                "Please select a mode using -f (for file) or -i (for interactive)"
            )
        if args.file:
            return self.read_file(args.file)
        else:
            return self.read_console()

    def read_file(self, path):  # -f argument
        # example of line from the input file: "Gaspard\t10\t30\t...\n"
        candidates = []
        try:
            data = open(path)
        except:
            sys.exit('Error: cannot open file "' + path + '"')
        for line in data:
            values = line.split("\t")
            votes = np.array(values[1:]).astype(int)
            candidates.append(Candidate(values[0], votes))
        return candidates

    def read_console(self):  # -i argument
        n_voters = int(input("Number of voters: "))
        n_candidates = int(input("Number of candidates: "))
        votes = np.zeros((n_candidates, 6))
        names = [input("Candidate %s's name: " % (i + 1)) for i in range(n_candidates)]
        for i in range(n_voters):
            print(
                "\nVoter " + str(i + 1) + ", it's your turn to give your opinion.\n"
                "Please, select one of the five following options for each candidate:\n"
                "\t5: Excellent\n\t4: Very Good\n\t3: Good\n\t2: Acceptable\n\t1: Poor\n\t0: Reject\n"
            )
            for j in range(n_candidates):
                try:
                    vote = np.clip(int(getpass(names[j] + ": ")), 0, 5)  # hide input
                except:
                    vote = 0
                votes[j][5 - vote] += 1
        candidates = []
        for i in range(n_candidates):
            candidates.append(Candidate(names[i], votes[i]))
        return candidates


class Candidate:
    def __init__(self, name, votes=[]):
        self.name = name
        self.ratings = votes / sum(votes)  # normalize
        self.is_winner = False

    def __str__(self):
        bar = ""
        category = 0
        for i in range(N_COLUMNS):
            while self.ratings.cumsum()[category] <= i / N_COLUMNS:
                category += 1
            # draw a line for the median
            symbol = "║" if i == int(N_COLUMNS / 2) else "█"
            bar += colored(symbol, COLORS[category])
        # print the winer in bold
        name = colored(self.name, attrs=["reverse", "bold"] if self.is_winner else None)
        return bar + " " + name

    def score(self):
        previous = 0
        for category, e in enumerate(self.ratings.cumsum()):
            if e >= 0.5:
                # If the majority categories are equal, we choose the one that has the most
                # votes above the majority category.
                # Since the ratings are normalized, we can directly substract the proportion of votes
                # above the majority category to get the score.
                return category - previous if category else 0
            previous = e


class MajorityJudgement:
    def __init__(self, candidates):
        candidates.sort(key=Candidate.score)
        best_score = min(candidates, key=Candidate.score).score()
        for i, candidate in enumerate(candidates):
            if candidate.score() == best_score:
                candidate.is_winner = True
        self.candidates = candidates

    def print_results(self):
        for candidate in self.candidates:
            print(candidate)


# Main
candidates = Interface().read()
MajorityJudgement(candidates).print_results()
