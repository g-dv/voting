# Majority Judgement

## Abstract

This script implements the [Majority Judgement](https://en.wikipedia.org/wiki/Majority_judgment) voting method.

## Demo

### File input mode

![Demo](./demo-file.gif)

### Interactive input mode

![Demo](./demo-interactive.gif)

## Usage

The scripts are written in Python 3. The `-h` argument will display the help.

### Cli

To let the voters type their vote directly in the console, use the `-i` argument.

**Example:** `python3 majority_judgement.py -i`

### Input file

A file can be provided as input when using the argument `-f`.

**Example:** `python3 majority_judgement.py -f <file>`

The file should respect the following format:
 * One line per candidate
 * There are 7 columns separated by tabs
 * The first column should contain the name of the candidate
 * The second column should contain the number of people who voted "Excellent"
 * The third column should contain the number of people who voted "Very Good"
 * etc.

**Example:**
```
Alice	221	112	130	224	115	198
Nestor	45	261	258	62	95	279
Oscar	64	241	249	84	84	278
```
