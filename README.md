# Voting

This repo contains two separate voting-related Python 3 projects:

* [Majority Judgement](./majority-judgement/README.md): an implementation of the method.
* [Iterative Voting](./iterative-voting/README.md): an experimental voting method.