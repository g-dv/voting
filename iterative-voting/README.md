# Iterative Voting System

**Table of Contents**

- [Iterative Voting System](#iterative-voting-system)
  - [Abstract](#abstract)
    - [Problem](#problem)
      - [Idea 1](#idea-1)
      - [Idea 2](#idea-2)
    - [Proposed solution](#proposed-solution)
  - [Usage](#usage)
  - [Demo](#demo)

## Abstract

### Problem

Let's say that you are part of a team that needs to choose a project. You may choose any project of a given list. The members of the team naturally do not agree on the project to choose.

#### Idea 1

So you have an idea: everyone should vote for one project or more, and the project that gets the most votes wins.

But, you realize that the members of the team are voting strategically, taking advantage of the votes of the others. You think that it's not really fair for those who don't want to use tricks like that.

#### Idea 2

So you have another idea: every member of the team should give a grade to each project. The project that gets the highest average grade wins.

But you have the same problem. For example, someone might give 100% to his favorite project and 0% to every other project, in order to have a greater influence on the average grades.

### Proposed solution

You go for _idea 1_, except you let people change their vote as many times as they want in order to maximize their satisfaction. If the situation converges, then you have your project!

Of course, you will use an algorithm to simulate this.

So, you ask people to give a grade to each project, like you did before (_idea 2_), and you run an algorithm that will try for each participant to maximize its score by changing its vote until it reaches a point where it can no longer improve the satisfaction of the participant.

This is what the code of this repo does.

## Usage

1. Fill the input file with a matrix of numbers separated by spaces.
   - One row per participant
   - One column per item
2. Run the python3 script `simulation.py` and pass the path of the input file as argument.
   - eg. `python3 simulation.py input.txt`

The script called `generate-random-input.py` takes two arguments: the number of participants and the number of items to vote for.
The script will print a random input file that you can use to test the algorithm.

## Demo

![Demo](./demo.gif)
