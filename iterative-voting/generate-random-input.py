from random import random
from sys import argv


def generate_preferences(n_items):
    """Generates the preferences of one voter.

    eg for n_items=4: [0.25, 0.5, 0.0, 0.25]
    """
    # generate a list of random numbers
    preferences = [random() for i in range(n_items)]
    # normalize
    preferences = [e / sum(preferences) for e in preferences]
    # convert as int
    return [int(e * 100000000) for e in preferences]


# generate preferences
n_voters = int(argv[1])
n_items = int(argv[2])

for preferences in [generate_preferences(n_items) for i in range(n_voters)]:
    for score in preferences[:-1]:
        print(score, end=" ")
    print(preferences[-1])
