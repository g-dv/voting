from sys import argv
from itertools import product


def get_score(preferences, votes):
    """Computes the satisfaction score of the voter using its preferences.

    The score is equal to the preference of the voter for the item that is
    winning the vote. If there are multiple items that are leading the vote,
    the score is the average of the preference for each of these items.
    (One way to see this is that in case of tie, we will pick the item randomly
    and therefore the voter should expect the average)
    """
    votes_per_item = get_votes_per_item(votes)
    score = 0
    for i in range(len(votes_per_item)):
        if votes_per_item[i] == max(votes_per_item):
            score += preferences[i] / votes_per_item.count(max(votes_per_item))
    return score


def get_votes_per_item(votes):
    """Gives a list that contains the number of votes for each item"""
    votes_per_item = [0] * len(votes[0])
    for i in range(len(votes)):
        for j in range(len(votes_per_item)):
            votes_per_item[j] += votes[i][j]
    return votes_per_item


def contains_favorite_items(vote, voter):
    """Skips the vote if it contains a 0 for the favorite items of the voter"""
    for j, e in enumerate(vote):
        if voter[j] == max(voter) and not e:
            return False
    return True


# read the input file given in argument
voters = [[int(e) for e in line.split(" ")] for line in open(argv[1])]
# normalize
voters = [[e / sum(voter) for e in voter] for voter in voters]
n_voters = len(voters)
n_items = len(voters[0])

# generate once and for all the different ways to vote
# eg. if there are 2 items: [0, 0], [0, 1], [1, 0], [1, 0]
# a "1" at position "i" means that we vote for item i
# (we can vote for multiple items)
possible_votes = list(product([0, 1], repeat=n_items))

# init. the votes to 0
votes = [[0 for j in range(n_items)] for i in range(n_voters)]
previous_votes = []

# print the preferences
print("Preferences:")
for voter in voters:
    print("   ", [f"{int(100 * score):02d}" for score in voter])

epoch = 0
while previous_votes != votes:
    # If the votes did not change, then they won't change anymore and therefore the
    # simulation should end.
    previous_votes = [e for e in votes]
    # for each voter, update its vote
    for i, voter in enumerate(voters):
        # init with the current situation (its previous vote)
        best_score = get_score(voter, votes)
        best_vote = votes[i]
        # calculate the score for each possible vote
        for possible_vote in possible_votes:
            # if the player has not voted for his favorite item, skip
            if not contains_favorite_items(possible_vote, voter):
                continue
            votes[i] = possible_vote
            score = get_score(voter, votes)
            # update the vote if necessary
            if score > best_score:
                best_score = score
                best_vote = possible_vote
        votes[i] = best_vote
    # print the situation
    epoch += 1
    print(f"Epoch {epoch}:")
    for i, vote in enumerate(votes):
        print(f"    {i}:", vote)
    print("    =:", get_votes_per_item(votes))
    if epoch >= 30:
        break
print(
    "Items with the most votes:\n   ",
    [
        i
        for i, n_votes in enumerate(get_votes_per_item(votes))
        if n_votes == max(get_votes_per_item(votes))
    ],
)
